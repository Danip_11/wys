package com.dosa.wys.musicrecognition.Auxiliar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dosa.wys.musicrecognition.Entities.Musica;
import com.dosa.wys.musicrecognition.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Adaptador extends FirebaseRecyclerAdapter<Musica, Adaptador.myViewHolder> {

    private boolean esFav;
    private FirebaseDatabase basededatos = FirebaseDatabase.getInstance();
    private DatabaseReference miBBDD = basededatos.getReference("Musica");

    public Adaptador(@NonNull FirebaseRecyclerOptions<Musica> opciones) {
        super(opciones);
    }

    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int posicion, @NonNull Musica modelo) {
        holder.tituloText.setText(modelo.getTitulo());
        holder.artistaText.setText(modelo.getArtista());
        //Picasso.get().load(modelo.getUrl()).into(holder.img1);
        //Glide.with(holder.img1.getContext()).load(modelo.getUrl()).into(holder.img1);

    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int tipoVista) {
        View vista= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_estilo,parent,false);
        return new myViewHolder(vista);
    }

    public class myViewHolder extends RecyclerView.ViewHolder {

        ImageView img1, btn_eliminar, btn_fav, btn_compartir;
        TextView tituloText, artistaText;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);

            img1=itemView.findViewById(R.id.img1);
            tituloText=itemView.findViewById(R.id.tituloText);
            artistaText=itemView.findViewById(R.id.artistaText);

            tituloText.setSelected(true);
            artistaText.setSelected(true);

            btn_eliminar = itemView.findViewById(R.id.imageDelete);
            btn_fav = itemView.findViewById(R.id.imageFav);
            btn_compartir = itemView.findViewById(R.id.imageShare);

            btn_eliminar.setClickable(true);

            btn_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    Toast.makeText(v.getContext(),
                            "Canción borrada",
                            Toast.LENGTH_LONG).show();

                }
            });

            btn_fav.setClickable(true);

            btn_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(v.getContext(),
                            "Favoritos",
                            Toast.LENGTH_LONG).show();

                    esFav = true;
                }

            });

            btn_compartir.setClickable(true);

            btn_compartir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(v.getContext(),
                            "Compartir",
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }

}
