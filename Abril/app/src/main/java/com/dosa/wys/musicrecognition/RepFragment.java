package com.dosa.wys.musicrecognition;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.dosa.wys.musicrecognition.Auxiliar.BuscadorYoutube;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class RepFragment extends Fragment {
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        return inflater.inflate(R.layout.fragment_rep, container, false);
    }

    public void onViewCreated(@NonNull View vista, Bundle savedInstanceState) {
        super.onViewCreated(vista, savedInstanceState);
        String videoId="";
        BuscadorYoutube buscadorYoutube = new BuscadorYoutube();
        URL url = null;
        try {
            url = new URL("https://www.googleapis.com/youtube/v3/search?part=snippet&q=hola%20video&maxResults=1&key=AIzaSyBGo3v2TCYDPzxd2zei3yQKszBvAlCfXnA");
            //max 1 https://www.googleapis.com/youtube/v3/search?part=snippet&q=hola%20video&maxResults=1&key=AIzaSyBGo3v2TCYDPzxd2zei3yQKszBvAlCfXnA
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        videoId = buscadorYoutube.buscarYT("https://www.googleapis.com/youtube/v3/search?part=snippet&q=hola%20video&maxResults=1&key=AIzaSyBGo3v2TCYDPzxd2zei3yQKszBvAlCfXnA");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
            //Info: https://stackoverflow.com/questions/36107697/update-modify-an-xml-file-in-android
            Document doc = builder.parse(getContext().openFileInput("../res/layout/fragment_rep.xml"));
            NodeList nodeslist = doc.getElementsByTagName("com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView");
            for(int i = 0 ; i < nodeslist.getLength() ; i ++){
                Node node = nodeslist.item(i);
                NamedNodeMap att = node.getAttributes();
                int h = 0;
                boolean isKIA= false;
                while( h < att.getLength()) {
                    Node yt= att.item(h);
                    if(yt.getNodeValue().equals("videoId")) {
                        yt.setNodeValue(videoId);
                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                        Transformer transformer = transformerFactory.newTransformer();
                        DOMSource dSource = new DOMSource(doc);
                        StreamResult result = new StreamResult(getContext().openFileOutput("../res/layout/fragment_rep.xml", Context.MODE_PRIVATE));  // To save it in the Internal Storage
                        transformer.transform(dSource, result);
                    }
                    h += 1;  // To get The Next Attribute.
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}