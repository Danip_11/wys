package com.dosa.wys.musicrecognition;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private Toolbar barraTareas;

    private String idUsuario;

    private BottomNavigationView menuNavBoton;
    private InicioFragment inicioFragment;
    private FavoritoFragment favoritoFragment;
    private HistorialFragment historyFragment;
    private BuscarFragment buscarFragment;
    private RepFragment repFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.temaAplicacion);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        barraTareas = findViewById(R.id.barraHerramientasMenu);
        setSupportActionBar(barraTareas);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        menuNavBoton = findViewById(R.id.menuNavBoton);


        inicioFragment = new InicioFragment();
        favoritoFragment = new FavoritoFragment();
        historyFragment = new HistorialFragment();
        buscarFragment = new BuscarFragment();
        repFragment = new RepFragment();

        replaceFragment(inicioFragment);

        menuNavBoton.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch(item.getItemId()) {

                    case R.id.accion_boton_inicio:
                        replaceFragment(inicioFragment);
                        return true;
                    case R.id.accion_boton_fav:
                        replaceFragment(favoritoFragment);
                        return true;
                    case R.id.accion_boton_historial:
                        replaceFragment(historyFragment);
                        return true;
                    case R.id.accion_boton_buscar:
                        replaceFragment(buscarFragment);
                        return true;
                    case R.id.accion_boton_rep:
                        replaceFragment(repFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    private void replaceFragment(Fragment fragment) {

        FragmentTransaction transicionFragment = getSupportFragmentManager().beginTransaction();
        transicionFragment.replace(R.id.contenedor_menu,fragment);
        transicionFragment.commit();

    }
}
