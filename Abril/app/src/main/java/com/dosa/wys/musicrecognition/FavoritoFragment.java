package com.dosa.wys.musicrecognition;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dosa.wys.musicrecognition.Auxiliar.Adaptador;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritoFragment extends Fragment {


    private Adaptador adaptador;
    private FirebaseDatabase basededatos = FirebaseDatabase.getInstance();
    private DatabaseReference miBBDD = basededatos.getReference("musica");

    public FavoritoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favoritos, container, false);
    }

}
