package com.dosa.wys.musicrecognition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Objects;

public class RegistrarActivity extends AppCompatActivity {

    private EditText nombreUsuario, correo, contra;
    private Button registrarBtn;
    private ProgressBar barraProgreso;
    private FirebaseAuth autentFirebase;
    private DatabaseReference referenciaBD;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        autentFirebase = FirebaseAuth.getInstance();
        nombreUsuario = findViewById(R.id.textName);
        correo = findViewById(R.id.textEmail);
        contra = findViewById(R.id.textPass);
        registrarBtn = findViewById(R.id.registerBtn);
        barraProgreso = findViewById(R.id.progressbar);
        registrarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vista) {
               startActivity(new Intent(RegistrarActivity.this, MainActivity.class));

            }
        });
    }

    private void registrar(final String nombreUsuario, final String correo, final String contra) {
        barraProgreso.setVisibility(View.VISIBLE);
        autentFirebase.createUserWithEmailAndPassword(correo,contra).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser bdUsuario = autentFirebase.getCurrentUser();
                    String IdUsuario;
                    IdUsuario = bdUsuario.getUid();
                    referenciaBD = FirebaseDatabase.getInstance().getReference("Usuarios").child(IdUsuario);
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("IdUsuario",IdUsuario);
                    hashMap.put("nombreUsuario",nombreUsuario);
                    hashMap.put("correo",correo);
                    hashMap.put("contra",contra);
                    hashMap.put("imagenUrl","default");
                    referenciaBD.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> tarea) {
                            barraProgreso.setVisibility(View.GONE);
                            if (tarea.isSuccessful()){
                                Intent intent = new Intent(RegistrarActivity.this,MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }else{
                                Toast.makeText(RegistrarActivity.this, Objects.requireNonNull(tarea.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(RegistrarActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}