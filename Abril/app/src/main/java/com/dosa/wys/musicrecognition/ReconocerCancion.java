package com.dosa.wys.musicrecognition;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.acrcloud.rec.sdk.ACRCloudClient;
import com.acrcloud.rec.sdk.ACRCloudConfig;
import com.acrcloud.rec.sdk.IACRCloudListener;
import com.airbnb.lottie.LottieAnimationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

public class ReconocerCancion extends AppCompatActivity implements IACRCloudListener {


    private ACRCloudClient conexionCliente;
    private ACRCloudConfig configuracion;

    HistorialFragment hf;

    private TextView volumenTexto;
    private TextView resultadoTexto;

    private boolean procesoCarga = false;
    private boolean estadoInicial = false;

    private String ruta = "";
    private String tres, src;


    private long tiempoInicio = 0;
    private long tiempoParar = 0;

    private boolean respuestaEncontrada = false;

    private Button btnComenzar;

    private ImageView cerrarBotonReconocer;

    private LottieAnimationView animacionJSON;

    String titulo, artista;

    FrameLayout falloReconocer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconocer);

        permisosNecesitados(this, android.Manifest.permission.RECORD_AUDIO);
        permisosNecesitados(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        ruta = Environment.getExternalStorageDirectory().toString()
                + "/acrcloud/model";

        File fichero = new File(ruta);
        if(!fichero.exists()){
            fichero.mkdirs();
        }

        falloReconocer = findViewById(R.id.activity_reconocer);

        if (savedInstanceState == null) {
            falloReconocer.setVisibility(View.INVISIBLE);

            ViewTreeObserver viewTreeObserver = falloReconocer.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("ObsoleteSdkInt")
                    @Override
                    public void onGlobalLayout() {
                        iniciarAnimacion();
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            falloReconocer.getViewTreeObserver();
                        } else {
                            falloReconocer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    }
                });
            }
        }

        volumenTexto = findViewById(R.id.volumenTexto);
        resultadoTexto = findViewById(R.id.resultadoTexto);

        btnComenzar = findViewById(R.id.btnEmpezarReconocer);
        btnComenzar.setText(getResources().getString(R.string.start));

        cerrarBotonReconocer = findViewById(R.id.cerrar_activity);

        animacionJSON = findViewById(R.id.localizacionAnim);

        cerrarBotonReconocer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        btnComenzar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                animacionJSON.setVisibility(View.VISIBLE);
                /*startBtn.setVisibility(View.INVISIBLE);*/

                btnComenzar.setVisibility(View.INVISIBLE);
                btnComenzar.startAnimation(cerrarAnimacion(false));

                /*cancelBtn.setVisibility(View.VISIBLE);*/

                respuestaEncontrada = false;

                start();
            }
        });

        /*cancelBtn.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        startBtn.setVisibility(View.VISIBLE);
                        cancelBtn.setVisibility(View.INVISIBLE);

                        cancel();
                    }
                });*/


        this.configuracion = new ACRCloudConfig();
        this.configuracion.acrcloudListener = this;

        // Si se implementa IACRCloudResultWithAudioListener y se inicia "onResult(ACRCloudResult result)", tendremos el dato de audio de la canción

        this.configuracion.context = this;
        this.configuracion.host = "identify-eu-west-1.acrcloud.com";
        this.configuracion.dbPath = ruta;
        this.configuracion.accessKey = "\t\n" + "1837639ba382d33e4842101623f0e92b";
        this.configuracion.accessSecret = "WDGz72slNCMihtENdiqdgF5g8sXBbpMBzLLzBU93";
        this.configuracion.protocol = ACRCloudConfig.ACRCloudNetworkProtocol.PROTOCOL_HTTP; // PROTOCOL_HTTPS
        this.configuracion.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_REMOTE;
        this.conexionCliente = new ACRCloudClient();
        // initWithConfig para usar sin conexión
        this.estadoInicial = this.conexionCliente.initWithConfig(this.configuracion);
        if (this.estadoInicial) {
            this.conexionCliente.startPreRecord(3000); //empieza a "prereconocer"
        }

        start();

        if(!respuestaEncontrada)
        {
            btnComenzar.setVisibility(View.INVISIBLE);
        }
        else {
            btnComenzar.setVisibility(View.VISIBLE);
        }

    }

    public static void permisosNecesitados(Activity activityUsada, String permisos) {
        if (ContextCompat.checkSelfPermission(activityUsada, permisos)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activityUsada, new String[]{permisos}, 0);
        }
    }

    @SuppressLint("SetTextI18n")
    public void start() {

        animacionJSON.playAnimation();
        if (!this.estadoInicial) {
            Toast.makeText(this, "Error de inicio", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!procesoCarga) {
            procesoCarga = true;
            volumenTexto.setText("");
            resultadoTexto.setText(R.string.listening);
            if (this.conexionCliente == null || !this.conexionCliente.startRecognize()) {
                procesoCarga = false;
                resultadoTexto.setText("Error de conexión");
            }
            tiempoInicio = System.currentTimeMillis();
        }
    }

    @SuppressLint("SetTextI18n")
    protected void cancel() {
        if (procesoCarga && this.conexionCliente != null) {
            procesoCarga = false;
            this.conexionCliente.cancel();
            resultadoTexto.setText("No se encontraron resultados, canceló el proceso");
        }
    }

    private Animation cerrarAnimacion(boolean fadeIn) {

        Animation animacion;
        if (fadeIn)
            animacion = new AlphaAnimation(0f, 1.0f);
        else
            animacion = new AlphaAnimation(1.0f, 0f);
        animacion.setDuration(500);
        animacion.setFillEnabled(true);
        animacion.setFillAfter(true);
        return animacion;

    }

    private void iniciarAnimacion() {

        int cx = falloReconocer.getWidth() / 2;
        int cy = falloReconocer.getHeight() / 2;

        float finalRadius = Math.max(falloReconocer.getWidth(), falloReconocer.getHeight());

        // se crea la animacion
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(falloReconocer, cx, cy, 0, finalRadius);
        circularReveal.setDuration(1000);

        // se ejecuta la animación y se hace visible
        falloReconocer.setVisibility(View.VISIBLE);
        circularReveal.start();
    }

    @Override
    public void onResult(String resultado) {
        if (this.conexionCliente != null) {
            this.conexionCliente.cancel();
            procesoCarga = false;
        }

        try {
            JSONObject resultadoJSON = new JSONObject(resultado);
            JSONObject estadoResultadoJSON = resultadoJSON.getJSONObject("status");
            int datosResultado = estadoResultadoJSON.getInt("code");
            hf = new HistorialFragment();
            if(datosResultado == 0){
                JSONObject metadata = resultadoJSON.getJSONObject("metadata");

                    JSONArray musicaArray = metadata.getJSONArray("music");
                    for(int i=0; i<1; i++) {
                        JSONObject buscarNumCancion = (JSONObject) musicaArray.get(i);
                        titulo = buscarNumCancion.getString("title");
                        JSONArray artistaACR = buscarNumCancion.getJSONArray("artists");
                        JSONObject artistaPosicion = (JSONObject) artistaACR.get(0);
                        artista = artistaPosicion.getString("name");
                        tres =  " Título: " + titulo + "\n" + "Artista: " + artista;
                        //String src_img = getWebsite();
                        hf.listaRecientes(titulo, artista);
                    }

                /*tres = tres + "\n\n" + resultado;*/
            }else if(datosResultado == 1001){
                tres = "No se encontraron resultados";
            }
        } catch (JSONException e) {
            tres = resultado;
            e.printStackTrace();
        }

        resultadoTexto.setText(tres);

        respuestaEncontrada = true;

        btnComenzar.setVisibility(View.VISIBLE);
        btnComenzar.startAnimation(cerrarAnimacion(true));
//        cancelBtn.setVisibility(View.INVISIBLE);
        animacionJSON.setProgress(0f);
        animacionJSON.pauseAnimation();
        animacionJSON.setVisibility(View.INVISIBLE);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onVolumeChanged(double volumen) {
        long tiempoEspera = (System.currentTimeMillis() - tiempoInicio) / 1000;
        volumenTexto.setText(tiempoEspera + " s");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("MainActivity", "release");
        if (this.conexionCliente != null) {
            this.conexionCliente.release();
            this.estadoInicial = false;
            this.conexionCliente = null;
        }
    }

    public String getWebsite() throws IOException {

            String artista = this.artista.replace(" ", "+");
            String srcimg = "https://www.shutterstock.com/es/search/" + artista + "+band";

            Document doc = Jsoup.connect(srcimg).get();
            Elements link = doc.select("img");

            //builder.append(link.attr("src")).append(link.hasClass("rg_i Q4LuWd"));

            src = link.attr("src");

        return src;
    }


}
