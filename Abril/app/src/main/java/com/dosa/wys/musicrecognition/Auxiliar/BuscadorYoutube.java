package com.dosa.wys.musicrecognition.Auxiliar;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BuscadorYoutube {
    //Info: https://github.com/sanjeeb-sang/Search-Youtube-Android-App/blob/104c0a3e5d49d4584b8befa0858566575ed018a3/app/src/main/java/sangraula/sanjeeb/wissionapp/utils/YoutubeSearchHelper.java#L47
      public final URL getRequestUrl(String searchFor) {

          URL url = null;

          try {

              url = new URL("https://www.googleapis.com/youtube/v3/search?part=snippet&q=hola%20video&maxResults=1&key=AIzaSyBGo3v2TCYDPzxd2zei3yQKszBvAlCfXnA");

          } catch (MalformedURLException e) {
              e.printStackTrace();
          }

          return url;

      }

    public String buscarYT(String searchFor) {

        URL url = getRequestUrl(searchFor);

        TareaBuscar st = new TareaBuscar();

        return String.valueOf(st.execute(url));
    }

    private class TareaBuscar extends AsyncTask<URL, Void, String> {

        @Override
        protected String doInBackground(URL... urls) {
            String videoId = "";
            URL url = urls[0];

            StringBuilder response = new StringBuilder();

            HttpURLConnection httpconn = null;

            try {
                httpconn = (HttpURLConnection) url.openConnection();

                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    input.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            try {

                JSONObject o1 = new JSONObject(response.toString());
                JSONArray a1 = o1.getJSONArray("items");
                JSONObject o2 = a1.getJSONObject(0);
                videoId = o2.getJSONObject("id").getString("videoId");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return videoId;
        }


    }
}