package com.dosa.wys.musicrecognition;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dosa.wys.musicrecognition.Auxiliar.Adaptador;
import com.dosa.wys.musicrecognition.Entities.Musica;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */

public class HistorialFragment extends Fragment {

    private RecyclerView VistaLista;
    private FirebaseDatabase basededatos = FirebaseDatabase.getInstance();
    private DatabaseReference miBBDD = basededatos.getReference("Musica");
    private Adaptador adaptador;
    private static final String TAG = "DocSnippets";


    public void listaRecientes(String titulo, String artista) {

        miBBDD.push().setValue(new Musica(titulo, artista, ""));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_historial, container, false);

        VistaLista = vista.findViewById(R.id.listView);
        VistaLista.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<Musica> opciones =
                new FirebaseRecyclerOptions.Builder<Musica>()
                        .setQuery(FirebaseDatabase.getInstance().getReference().child("Musica"), Musica.class)
                        .build();

        adaptador =new Adaptador(opciones);
        VistaLista.setAdapter(adaptador);

        return vista;

    }

    @Override
    public void onStart() {
        super.onStart();
        adaptador.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adaptador.stopListening();
    }

    /*private String getWebsite() {

                try {
                    Document doc = Jsoup.connect("https://www.shutterstock.com/es/search/offspring+band").get();
                    Elements link;
                    link = doc.select("img");

                    //builder.append(link.attr("src")).append(link.hasClass("rg_i Q4LuWd"));

                    src = link.attr("src");


                }catch(IOException e){
                    src = "";
                }

        return src;
    }*/

}
