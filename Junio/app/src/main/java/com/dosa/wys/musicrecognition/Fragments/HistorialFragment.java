package com.dosa.wys.musicrecognition.Fragments;

import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dosa.wys.musicrecognition.Auxiliar.Adaptador;
import com.dosa.wys.musicrecognition.Auxiliar.ConexionSQLite;
import com.dosa.wys.musicrecognition.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */

public class HistorialFragment extends Fragment {

    RecyclerView recyclerView;
    ConexionSQLite miBD;
    ArrayList<String> titulo, artista, imagen, idVideo, favorito, oculto;
    Adaptador adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_historial, container, false);
        recyclerView = vista.findViewById(R.id.listView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        miBD = new ConexionSQLite(getContext());
        idVideo = new ArrayList<>();
        titulo = new ArrayList<>();
        artista = new ArrayList<>();
        imagen = new ArrayList<>();
        favorito = new ArrayList<>();
        oculto = new ArrayList<>();

        consultarRecientes();

        adaptador = new Adaptador(getContext(), idVideo, titulo, artista, imagen, favorito, oculto,"hf");
        recyclerView.setAdapter(adaptador);

        return vista;

    }

    private void consultarRecientes() {
        Cursor cursor = miBD.leerTabla(0);
        if (cursor.getCount() == 0) {
        } else {
            while (cursor.moveToNext()) {
                idVideo.add(cursor.getString(0));
                titulo.add(cursor.getString(1));
                artista.add(cursor.getString(2));
                imagen.add(cursor.getString(3));
                favorito.add(cursor.getString(4));
                oculto.add(cursor.getString(5));
            }
        }
    }
}
