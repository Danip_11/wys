package com.dosa.wys.musicrecognition.Activities;

import android.os.Bundle;

import com.dosa.wys.musicrecognition.Fragments.BottomSheetFragmentYT;
import com.dosa.wys.musicrecognition.Fragments.FavoritoFragment;
import com.dosa.wys.musicrecognition.Fragments.HistorialFragment;
import com.dosa.wys.musicrecognition.Fragments.InicioFragment;
import com.dosa.wys.musicrecognition.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements BottomSheetFragmentYT.BottomSheetListener{

    private Toolbar barraTareas;

    private BottomNavigationView menuNavBoton;
    private InicioFragment inicioFragment;
    private FavoritoFragment favoritoFragment;
    private HistorialFragment historialFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity_menu);

        barraTareas = findViewById(R.id.barraHerramientasMenu);
        setSupportActionBar(barraTareas);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        menuNavBoton = findViewById(R.id.menuNavBoton);


        inicioFragment = new InicioFragment();
        favoritoFragment = new FavoritoFragment();
        historialFragment = new HistorialFragment();

        replaceFragment(inicioFragment,"if");


    }

    @Override
    protected void onStart() {
        super.onStart();

        menuNavBoton.setOnNavigationItemSelectedListener(item -> {

            switch(item.getItemId()) {

                case R.id.accion_boton_inicio:
                    replaceFragment(inicioFragment,"if");
                    return true;
                case R.id.accion_boton_fav:
                    replaceFragment(favoritoFragment,"ff");
                    return true;
                case R.id.accion_boton_historial:
                    replaceFragment(historialFragment,"hf");
                    return true;
                default:
                    return false;
            }
        });

    }

    public void replaceFragment(Fragment fragment,String tag) {
        FragmentTransaction transicionFragment = getSupportFragmentManager().beginTransaction();
        transicionFragment.replace(R.id.contenedor_menu,fragment,tag);
        transicionFragment.commit();
    }

    @Override
    public void onButtonClicked(String text) {

    }
}
