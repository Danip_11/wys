package com.dosa.wys.musicrecognition.Entities;

public class Musica {

    public String titulo,artista,url;
    public boolean isFav;

    public Musica() {
    }

    public Musica(String titulo, String artista, String url, boolean isFav) {

        this.titulo = titulo;
        this.artista = artista;
        this.url=url;
        this.isFav=isFav;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public boolean isFav() { return isFav; }

    public void setFav(boolean fav) { isFav = fav; }

}
