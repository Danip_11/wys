package com.dosa.wys.musicrecognition;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 */

public class RepFragment extends Fragment {

    private Button getBtn;
    private TextView result;
    private String src;

    public RepFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rep, container, false);

        result = (TextView) view.findViewById(R.id.result);
        getBtn = (Button) view.findViewById(R.id.getBtn);
        getWebsite();

        return view;
    }

    private void getWebsite() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect("https://www.shutterstock.com/es/search/offspring+band").get();
                    Elements link;
                    link = doc.select("img");

                        //builder.append(link.attr("src")).append(link.hasClass("rg_i Q4LuWd"));

                    src = link.attr("src");

                    builder.append(src);

                }catch(IOException e){
                    builder.append("Error: ").append(e.getMessage()).append("\n");
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        result.setText(builder.toString());
                    }
                });

            }
        }).start();

    }
}