package com.dosa.wys.musicrecognition.Auxiliar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dosa.wys.musicrecognition.Entities.Musica;
import com.dosa.wys.musicrecognition.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Adapter extends FirebaseRecyclerAdapter<Musica,Adapter.myViewHolder> {

    private boolean isFav;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myBBDD = database.getReference("musica");

    public Adapter(@NonNull FirebaseRecyclerOptions<Musica> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int position, @NonNull Musica model) {
        holder.tituloText.setText(model.getTitulo());
        holder.artistaText.setText(model.getArtista());
        //Picasso.get().load(model.getUrl()).into(holder.img1);
        Glide.with(holder.img1.getContext()).load(model.getUrl()).into(holder.img1);

    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.singlerowdesign,parent,false);
        return new myViewHolder(view);
    }

    public class myViewHolder extends RecyclerView.ViewHolder {

        ImageView img1, btn_delete, btn_fav, btn_share;
        TextView tituloText, artistaText;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);

            img1=itemView.findViewById(R.id.img1);
            tituloText=itemView.findViewById(R.id.tituloText);
            artistaText=itemView.findViewById(R.id.artistaText);

            tituloText.setSelected(true);
            artistaText.setSelected(true);

            btn_delete = itemView.findViewById(R.id.imageDelete);
            btn_fav = itemView.findViewById(R.id.imageFav);
            btn_share = itemView.findViewById(R.id.imageShare);

            btn_delete.setClickable(true);

            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    Toast.makeText(v.getContext(),
                            "Canción borrada",
                            Toast.LENGTH_LONG).show();

                }
            });

            btn_fav.setClickable(true);

            btn_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(v.getContext(),
                            "Favoritos",
                            Toast.LENGTH_LONG).show();

                    isFav = true;
                }

            });

            btn_share.setClickable(true);

            btn_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(v.getContext(),
                            "Compartir",
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    public boolean isFav() {
        return isFav;
    }
}
