package com.dosa.wys.musicrecognition;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private Toolbar mainToolbar;

    private String current_user_id;

    private BottomNavigationView mainBottomNav;
    private HomeFragment homeFragment;
    private FavouriteFragment favouriteFragment;
    private HistoryFragment historyFragment;
    private SearchFragment searchFragment;
    private RepFragment repFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainToolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(mainToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainBottomNav = findViewById(R.id.mainBottomNav);


        homeFragment = new HomeFragment();
        favouriteFragment = new FavouriteFragment();
        historyFragment = new HistoryFragment();
        searchFragment = new SearchFragment();
        repFragment = new RepFragment();

        replaceFragment(homeFragment);

        mainBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch(item.getItemId()) {

                    case R.id.bottom_action_home:
                        replaceFragment(homeFragment);
                        return true;
                    case R.id.bottom_action_notification:
                        replaceFragment(favouriteFragment);
                        return true;
                    case R.id.bottom_action_account:
                        replaceFragment(historyFragment);
                        return true;
                    case R.id.bottom_action_search:
                        replaceFragment(searchFragment);
                        return true;
                    case R.id.bottom_action_rep:
                        replaceFragment(repFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    private void replaceFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,fragment);
        fragmentTransaction.commit();

    }
}
