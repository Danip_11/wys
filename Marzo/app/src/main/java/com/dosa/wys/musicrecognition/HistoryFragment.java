package com.dosa.wys.musicrecognition;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dosa.wys.musicrecognition.Auxiliar.Adapter;
import com.dosa.wys.musicrecognition.Entities.Musica;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */

public class HistoryFragment extends Fragment {

    private RecyclerView listView;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myBBDD = database.getReference("musica");
    private Adapter adapter;
    private static final String TAG = "DocSnippets";

    public void listaRecientes(String title, String artist, String src, boolean tof){

        myBBDD.push().setValue(new Musica(title, artist, src, tof));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        listView = view.findViewById(R.id.listView);
        listView.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<Musica> options =
                new FirebaseRecyclerOptions.Builder<Musica>()
                        .setQuery(FirebaseDatabase.getInstance().getReference().child("musica"), Musica.class)
                        .build();

        adapter=new Adapter(options);
        listView.setAdapter(adapter);

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    /*private String getWebsite() {

                try {
                    Document doc = Jsoup.connect("https://www.shutterstock.com/es/search/offspring+band").get();
                    Elements link;
                    link = doc.select("img");

                    //builder.append(link.attr("src")).append(link.hasClass("rg_i Q4LuWd"));

                    src = link.attr("src");


                }catch(IOException e){
                    src = "";
                }

        return src;
    }*/

}
