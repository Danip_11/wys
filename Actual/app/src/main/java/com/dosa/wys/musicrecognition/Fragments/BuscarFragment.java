package com.dosa.wys.musicrecognition.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.dosa.wys.musicrecognition.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class BuscarFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_buscar, container, false);
        /*
        Document doc = null;
        try {
            doc = Jsoup.connect("https://es.wikipedia.org/wiki/Alejandro_Sanz").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements table = doc.select(".infobox biography vcard td:eq(0)");
        for (Element e : table){
            System.out.println(e.text());
        }
        */
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}