package com.dosa.wys.musicrecognition.Auxiliar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.dosa.wys.musicrecognition.Activities.MainActivity;
import com.dosa.wys.musicrecognition.Activities.ReconocerCancion;
import com.dosa.wys.musicrecognition.Fragments.BottomSheetFragmentYT;
import com.dosa.wys.musicrecognition.Fragments.FavoritoFragment;
import com.dosa.wys.musicrecognition.Fragments.HistorialFragment;
import com.dosa.wys.musicrecognition.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<Adaptador.MusicaViewHolder> {

    private Context context;
    private ArrayList titulo, artista, imagen, idvideo, favorito, oculto;
    private String fragment;


    public Adaptador(Context context, ArrayList idvideo, ArrayList titulo, ArrayList artista,
                     ArrayList imagen, ArrayList favorito, ArrayList oculto, String fragment) {
        this.context = context;
        this.idvideo = idvideo;
        this.titulo = titulo;
        this.artista = artista;
        this.imagen = imagen;
        this.favorito = favorito;
        this.oculto = oculto;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public MusicaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int tipovista) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_estilo, parent, false);
        return new MusicaViewHolder(vista);
    }


    @Override
    public void onBindViewHolder(@NonNull MusicaViewHolder holder, int position) {
        holder.tituloText.setText(String.valueOf(titulo.get(position)));
        holder.artistaText.setText(String.valueOf(artista.get(position)));
        Picasso.get().load(String.valueOf(imagen.get(position))).into(holder.img);
        holder.idv = String.valueOf(idvideo.get(position));
        holder.fragment = fragment;
        holder.isFavorito = Integer.parseInt(String.valueOf(favorito.get(position)));
        if (holder.isFavorito == 1) {
            holder.favoritos.setColorFilter(Color.RED);
        }
    }

    @Override
    public int getItemCount() {
        return titulo.size();
    }

    public class MusicaViewHolder extends RecyclerView.ViewHolder {
        TextView tituloText, artistaText;
        ImageView img, borrarCancion, favoritos, compartir, reproducir;
        String idv, fragment;
        int isFavorito;

        public MusicaViewHolder(@NonNull View itemView) {
            super(itemView);
            ReconocerCancion rec = new ReconocerCancion();
            tituloText = itemView.findViewById(R.id.tituloText);
            artistaText = itemView.findViewById(R.id.artistaText);
            img = itemView.findViewById(R.id.img);
            borrarCancion = itemView.findViewById(R.id.imageDelete);
            favoritos = itemView.findViewById(R.id.imageFav);
            compartir = itemView.findViewById(R.id.imageShare);
            reproducir = itemView.findViewById(R.id.imagePlay);


            tituloText.setSelected(true);
            artistaText.setSelected(true);

            borrarCancion.setOnClickListener(v -> {
                ConexionSQLite myBD = new ConexionSQLite(v.getContext());
                myBD.ocultarCancion(idv, 1);
                AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                FragmentManager manager = activity.getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                if (fragment.equals("hf")) {
                    HistorialFragment historialFragment = new HistorialFragment();
                    ft.replace(R.id.fragmentHistorial, historialFragment);
                } else if (fragment.equals("ff")) {
                    FavoritoFragment favoritoFragment = new FavoritoFragment();
                    ft.replace(R.id.fragmentFavorito, favoritoFragment);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
                manager.executePendingTransactions();
                Toast.makeText(v.getContext(), "Canción borrada correctamente", Toast.LENGTH_SHORT).show();
            });

            favoritos.setOnClickListener(v -> {
                ConexionSQLite myBD = new ConexionSQLite(v.getContext());
                if (isFavorito == 0) {
                    if (myBD.anadirFavorito(idv, isFavorito)) {
                        Toast.makeText(v.getContext(), "Canción añadida a favoritos", Toast.LENGTH_SHORT).show();
                        favoritos.setColorFilter(Color.RED);
                    }
                } else {
                    if (myBD.anadirFavorito(idv, isFavorito)) {
                        AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                        FragmentManager manager = activity.getSupportFragmentManager();
                        FragmentTransaction ft = manager.beginTransaction();
                        FavoritoFragment favoritoFragment = new FavoritoFragment();
                        ft.replace(R.id.fragmentFavorito, favoritoFragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.addToBackStack(null);
                        ft.commit();
                        manager.executePendingTransactions();
                        Toast.makeText(v.getContext(), "Canción borrada de favoritos", Toast.LENGTH_SHORT).show();
                        favoritos.setColorFilter(Color.WHITE);
                    }
                }
            });

            compartir.setOnClickListener(v -> {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://www.youtube.com/watch?v=" + idv);
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, "Compartir con");
                context.startActivity(shareIntent);
            });

            reproducir.setOnClickListener(v -> {
                BottomSheetFragmentYT bottomSheet = new BottomSheetFragmentYT();
                AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                FragmentManager manager = activity.getSupportFragmentManager();
                bottomSheet.setIdV(idv);
                bottomSheet.show(manager, "bottomSheetYT");
            });
        }
    }

}