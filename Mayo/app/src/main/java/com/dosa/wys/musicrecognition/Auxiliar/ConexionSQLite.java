package com.dosa.wys.musicrecognition.Auxiliar;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.icu.text.SimpleDateFormat;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.dosa.wys.musicrecognition.Activities.ReconocerCancion;
import com.dosa.wys.musicrecognition.Fragments.HistorialFragment;
import com.dosa.wys.musicrecognition.R;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

public class ConexionSQLite extends SQLiteOpenHelper {
    private Context context;
    private static final String BBDD = "Wys.db";
    private static final int BBDD_VERSION = 1;
    private static final String TABLA = "Musica";
    private static final String COLUMNA_IDVIDEO = "Idvideo";
    private static final String COLUMNA_TITULO = "Titulo";
    private static final String COLUMNA_ARTISTA = "Artista";
    private static final String COLUMNA_IMAGEN = "Imagen";
    private static final String COLUMNA_FAVORITO = "Favorito";
    private static final String COLUMNA_OCULTO = "Oculto";
    private static final String COLUMNA_FECHA = "Fecha";

    public ConexionSQLite(@Nullable Context context) {
        super(context, BBDD, null, BBDD_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE " + TABLA +
                " (" + COLUMNA_IDVIDEO + " VARCHAR PRIMARY KEY NOT NULL," +
                COLUMNA_TITULO + " VARCHAR, " +
                COLUMNA_ARTISTA + " VARCHAR, " +
                COLUMNA_IMAGEN + " VARCHAR, " +
                COLUMNA_FAVORITO + " BOOLEAN, " +
                COLUMNA_OCULTO + " BOOLEAN, " +
                COLUMNA_FECHA + " DEFAULT CURRENT_TIMESTAMP NOT NULL);";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void anadirMusica(String idv, String titulo, String artista, String imagen, Boolean favorito, Boolean oculto) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        if (idv != "Error" && imagen != "Error") {
            if (isRepetida(idv) == false) {

                cv.put(COLUMNA_IDVIDEO, idv);
                cv.put(COLUMNA_TITULO, titulo);
                cv.put(COLUMNA_ARTISTA, artista);
                cv.put(COLUMNA_IMAGEN, imagen);
                cv.put(COLUMNA_FAVORITO, favorito);
                cv.put(COLUMNA_OCULTO, oculto);


                long result = database.insert(TABLA, null, cv);
                if (result == -1) {
                    Toast.makeText(context, "Error al guardar o canción ya reconocida", Toast.LENGTH_SHORT).show();
                    ReconocerCancion reconocerCancion = new ReconocerCancion();
                    //reconocerCancion.alertDialogCreate(titulo, artista);
                } else {
                    Toast.makeText(context, "Canción añadida correctamente a 'Recientes'", Toast.LENGTH_SHORT).show();
                    ReconocerCancion reconocerCancion = new ReconocerCancion();
                    //reconocerCancion.alertDialogCreate(titulo, artista);
                }
            } else {
                ocultarCancion(idv, 0);
            }
        } else {
            Toast.makeText(context, "Ups, parece que ha excedido la cuota de peticiones, pruebe más tarde", Toast.LENGTH_SHORT).show();
        }

    }

    public Cursor leerTabla(int tipolectura) {
        Cursor cursor = null;
        String query = "";
        SQLiteDatabase db;
        /*
           __________________________________________________
         ´                                                    `
        |   tipolectura = 0 -->  leer canciones no cultas      |
        |   tipolectura = 1 -->  leer canciones favoritas      |
        |   tipolectura = 2 -->  leer todas las canciones      |
         ` __________________________________________________ ´
        */

        switch (tipolectura) {
            case 0:
                query = " SELECT * FROM " + TABLA + " WHERE " + COLUMNA_OCULTO + " = " + 0 + " ORDER BY " + COLUMNA_FECHA + " DESC LIMIT 10";
                db = this.getReadableDatabase();

                if (db != null) {
                    cursor = db.rawQuery(query, null);
                }
                return cursor;
            case 1:
                query = " SELECT * FROM " + TABLA + " WHERE " + COLUMNA_FAVORITO + " = " + 1 + " AND " + COLUMNA_OCULTO + " = " + 0 + " ORDER BY " + COLUMNA_FECHA + " DESC";
                db = this.getReadableDatabase();

                if (db != null) {
                    cursor = db.rawQuery(query, null);
                }
                return cursor;
            default:
                return cursor;

        }
    }

    public void borrarFavoritos(String row_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(TABLA, COLUMNA_FAVORITO + " = " + 1, new String[]{row_id});
        if (result == -1) {
            Toast.makeText(context, "Error al borrar", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Borrado correctamente", Toast.LENGTH_SHORT).show();
        }
    }

    public void borrarTodo() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE " + TABLA);
        db.close();
    }

    public void ocultarCancion(String idVideo, int ocultar) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        if (ocultar == 1) {
            cv.put(COLUMNA_OCULTO, 1);
            db.update(TABLA, cv, COLUMNA_IDVIDEO + "=?", new String[]{idVideo});
            db.close();
        } else {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String tiempoActual = formato.format(new Date());
            cv.put(COLUMNA_OCULTO, 0);
            db.update(TABLA, cv, COLUMNA_IDVIDEO + "=?", new String[]{idVideo});
            cv.put(COLUMNA_FECHA, tiempoActual);
            db.update(TABLA, cv, COLUMNA_IDVIDEO + "=?", new String[]{idVideo});
            db.close();
        }
    }

    public boolean anadirFavorito(String idVideo, int isFavorito) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        if (isFavorito == 0) {
            cv.put(COLUMNA_FAVORITO, 1);
        } else {
            cv.put(COLUMNA_FAVORITO, 0);
        }

        db.update(TABLA, cv, COLUMNA_IDVIDEO + "=?", new String[]{idVideo});
        db.close();
        return true;

    }

    public boolean anadirRep(String idVideo) {
        Cursor cursor = null;


        String query = " SELECT * FROM " + TABLA + " WHERE " + COLUMNA_IDVIDEO + " = " + idVideo;
        SQLiteDatabase db = this.getReadableDatabase();

        cursor = db.rawQuery(query, null);


        return true;
    }

    @SuppressLint("Recycle")
    private boolean isRepetida(String idV) {
        String query = " SELECT * FROM " + TABLA + " WHERE " + COLUMNA_IDVIDEO + " =  '" + idV + "'";
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String idVideo = "";
        int oculto = 0;


        if (db != null) {
            cursor = db.rawQuery(query, null);
        }
        if (cursor.getCount() == 0) {
        } else {
            while (cursor.moveToNext()) {
                idVideo = cursor.getString(0);
                oculto = Integer.parseInt(cursor.getString(5));
            }
        }
        if (idVideo.equals(idV)) {
            if (oculto == 1) {
                return true;
            }
        }
        return false;
    }

}
