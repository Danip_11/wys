package com.dosa.wys.musicrecognition.Fragments;


import android.database.Cursor;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dosa.wys.musicrecognition.Auxiliar.Adaptador;
import com.dosa.wys.musicrecognition.Auxiliar.ConexionSQLite;
import com.dosa.wys.musicrecognition.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritoFragment extends Fragment {


    RecyclerView recyclerView;
    ConexionSQLite myDB;
    ArrayList<String> titulo, artista, imagen, idvideo, favorito, oculto;
    Adaptador adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_favoritos, container, false);
        recyclerView = vista.findViewById(R.id.listView2);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        myDB = new ConexionSQLite(getContext());
        idvideo = new ArrayList<>();
        titulo = new ArrayList<>();
        artista = new ArrayList<>();
        imagen = new ArrayList<>();
        favorito = new ArrayList<>();
        oculto = new ArrayList<>();

        consultarFavoritos();

        adaptador = new Adaptador(getContext(), idvideo, titulo, artista, imagen, favorito, oculto, "ff");
        recyclerView.setAdapter(adaptador);


        return vista;

    }

    private void consultarFavoritos() {
        Cursor cursor = myDB.leerTabla(1);
        if (cursor.getCount() == 0) {
        } else {
            while (cursor.moveToNext()) {
                idvideo.add(cursor.getString(0));
                titulo.add(cursor.getString(1));
                artista.add(cursor.getString(2));
                imagen.add(cursor.getString(3));
                favorito.add(cursor.getString(4));
                oculto.add(cursor.getString(5));
            }
        }
    }

}
