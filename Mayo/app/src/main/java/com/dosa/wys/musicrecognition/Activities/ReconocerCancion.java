package com.dosa.wys.musicrecognition.Activities;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.acrcloud.rec.sdk.ACRCloudClient;
import com.acrcloud.rec.sdk.ACRCloudConfig;
import com.acrcloud.rec.sdk.IACRCloudListener;
import com.airbnb.lottie.LottieAnimationView;
import com.dosa.wys.musicrecognition.Auxiliar.ConexionSQLite;
import com.dosa.wys.musicrecognition.Fragments.BottomSheetFragmentYT;
import com.dosa.wys.musicrecognition.Fragments.HistorialFragment;
import com.dosa.wys.musicrecognition.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;

public class ReconocerCancion extends AppCompatActivity implements IACRCloudListener, BottomSheetFragmentYT.BottomSheetListener {

    private ACRCloudClient conexionCliente;
    private ACRCloudConfig configuracion;
    public HistorialFragment hf;
    public BottomSheetFragmentYT bsfYT;
    private TextView tiempoTextoEspera, resultadoTexto;
    private boolean procesoCarga = false, estadoInicial = false, respuestaEncontrada = false;
    private String cancionInfo, titulo, artista, ruta = "";
    private long tiempoInicio = 0;
    private Button btnComenzar;
    private ImageView cerrarBotonReconocer;
    private LottieAnimationView animacionJSON;
    private FrameLayout falloReconocer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconocer);

        permisosNecesitados(this, android.Manifest.permission.RECORD_AUDIO);
        //permisosNecesitados(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        ruta = Environment.getExternalStorageDirectory().toString()
                + "/acrcloud/model";

        File fichero = new File(ruta);
        if (!fichero.exists()) {
            fichero.mkdirs();
        }

        falloReconocer = findViewById(R.id.activity_reconocer);

        if (savedInstanceState == null) {
            falloReconocer.setVisibility(View.INVISIBLE);

            ViewTreeObserver viewTreeObserver = falloReconocer.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("ObsoleteSdkInt")
                    @Override
                    public void onGlobalLayout() {
                        iniciarAnimacion();
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            falloReconocer.getViewTreeObserver();
                        } else {
                            falloReconocer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    }
                });
            }
        }

        tiempoTextoEspera = findViewById(R.id.tiempoTextoEspera);
        resultadoTexto = findViewById(R.id.resultadoTexto);

        btnComenzar = findViewById(R.id.btnEmpezarReconocer);
        btnComenzar.setText(getResources().getString(R.string.start));

        cerrarBotonReconocer = findViewById(R.id.cerrar_activity);

        animacionJSON = findViewById(R.id.localizacionAnim);

        cerrarBotonReconocer.setOnClickListener(v -> finish());

        btnComenzar.setOnClickListener(arg0 -> {

            animacionJSON.setVisibility(View.VISIBLE);

            btnComenzar.setVisibility(View.INVISIBLE);
            btnComenzar.startAnimation(cerrarAnimacion(false));


            respuestaEncontrada = false;

            start();
        });


        this.configuracion = new ACRCloudConfig();
        this.configuracion.acrcloudListener = this;

        // Si se implementa IACRCloudResultWithAudioListener y se inicia "onResult(ACRCloudResult result)", tendremos el dato de audio de la canción

        this.configuracion.context = this;
        this.configuracion.host = "identify-eu-west-1.acrcloud.com";
        this.configuracion.dbPath = ruta;
        this.configuracion.accessKey = "\t\n" + "1837639ba382d33e4842101623f0e92b";
        this.configuracion.accessSecret = "WDGz72slNCMihtENdiqdgF5g8sXBbpMBzLLzBU93";
        this.configuracion.protocol = ACRCloudConfig.ACRCloudNetworkProtocol.PROTOCOL_HTTP; // PROTOCOL_HTTPS
        this.configuracion.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_REMOTE;
        this.conexionCliente = new ACRCloudClient();
        // initWithConfig para usar sin conexión
        this.estadoInicial = this.conexionCliente.initWithConfig(this.configuracion);
        if (this.estadoInicial) {
            this.conexionCliente.startPreRecord(3000); //empieza a "prereconocer"
        }

        start();

        if (!respuestaEncontrada) {
            btnComenzar.setVisibility(View.INVISIBLE);
        } else {
            btnComenzar.setVisibility(View.VISIBLE);
        }

    }

    public static void permisosNecesitados(Activity activityUsada, String permisos) {
        if (ContextCompat.checkSelfPermission(activityUsada, permisos)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activityUsada, new String[]{permisos}, 0);
        }
    }

    @SuppressLint("SetTextI18n")
    public void start() {

        animacionJSON.playAnimation();
        if (!this.estadoInicial) {
            Toast.makeText(this, "Error de inicio", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!procesoCarga) {
            procesoCarga = true;
            tiempoTextoEspera.setText("");
            resultadoTexto.setText(R.string.listening);
            if (this.conexionCliente == null || !this.conexionCliente.startRecognize()) {
                procesoCarga = false;
                resultadoTexto.setText("Error de conexión");
            }
            tiempoInicio = System.currentTimeMillis();
        }
    }

    private Animation cerrarAnimacion(boolean fadeIn) {

        Animation animacion;
        if (fadeIn)
            animacion = new AlphaAnimation(0f, 1.0f);
        else
            animacion = new AlphaAnimation(1.0f, 0f);
        animacion.setDuration(500);
        animacion.setFillEnabled(true);
        animacion.setFillAfter(true);
        return animacion;

    }

    private void iniciarAnimacion() {

        int cx = falloReconocer.getWidth() / 2;
        int cy = falloReconocer.getHeight() / 2;

        float finalRadius = Math.max(falloReconocer.getWidth(), falloReconocer.getHeight());

        // se crea la animacion
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(falloReconocer, cx, cy, 0, finalRadius);
        circularReveal.setDuration(1000);

        // se ejecuta la animación y se hace visible
        falloReconocer.setVisibility(View.VISIBLE);
        circularReveal.start();
    }

    @Override
    public void onResult(String resultado) {
        if (this.conexionCliente != null) {
            this.conexionCliente.cancel();
            procesoCarga = false;
        }

        try {
            JSONObject resultadoJSON = new JSONObject(resultado);
            JSONObject estadoResultadoJSON = resultadoJSON.getJSONObject("status");
            int datosResultado = estadoResultadoJSON.getInt("code");
            hf = new HistorialFragment();
            bsfYT = new BottomSheetFragmentYT();
            if (datosResultado == 0) {
                JSONObject metadata = resultadoJSON.getJSONObject("metadata");

                JSONArray musicaArray = metadata.getJSONArray("music");
                for (int i = 0; i < 1; i++) {
                    JSONObject buscarNumCancion = (JSONObject) musicaArray.get(i);
                    titulo = buscarNumCancion.getString("title");
                    JSONArray artistaACR = buscarNumCancion.getJSONArray("artists");
                    JSONObject artistaPosicion = (JSONObject) artistaACR.get(0);
                    artista = artistaPosicion.getString("name");
                    artista = artista.replace(";",", ");
                    cancionInfo = " Título: " + titulo + "\n" + "Artista: " + artista;
                    ConexionSQLite myDB = new ConexionSQLite(getApplicationContext());
                    String idVideo = bsfYT.getInfo(titulo, artista, 0);
                    String imagen = bsfYT.getInfo(titulo, artista, 1);
                    myDB.anadirMusica(idVideo, titulo, artista, imagen,false, false);
                    alertDialogCreate(idVideo);
                }

            } else if (datosResultado == 1001) {
                cancionInfo = "No se encontraron resultados";
            }
        } catch (JSONException e) {
            cancionInfo = resultado;
            e.printStackTrace();
        }

        resultadoTexto.setText(cancionInfo);

        respuestaEncontrada = true;

        btnComenzar.setVisibility(View.VISIBLE);
        btnComenzar.startAnimation(cerrarAnimacion(true));
        animacionJSON.setProgress(0f);
        animacionJSON.pauseAnimation();
        animacionJSON.setVisibility(View.INVISIBLE);

    }

    public void alertDialogCreate(String idVideo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Quieres reproducir la canción?")
                .setTitle("Reproducir")
                .setPositiveButton("Sí", (dialog, id) -> {
                    BottomSheetFragmentYT bottomSheet = new BottomSheetFragmentYT();
                    FragmentManager manager = getSupportFragmentManager();
                    bottomSheet.setIdV(idVideo);
                    bottomSheet.show(manager, "bottomSheetYT");

                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {

                    finish();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                });
        builder.show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onVolumeChanged(double volumen) {
        long tiempoEspera = (System.currentTimeMillis() - tiempoInicio) / 1000;
        tiempoTextoEspera.setText(tiempoEspera + " s");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("MainActivity", "release");
        if (this.conexionCliente != null) {
            this.conexionCliente.release();
            this.estadoInicial = false;
            this.conexionCliente = null;
        }
    }

    @Override
    public void onButtonClicked(String text) {

    }
}
