package com.dosa.wys.musicrecognition.Auxiliar;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class BuscadorYoutube {

    public BuscadorYoutube() {
    }

    //Info: https://github.com/sanjeeb-sang/Search-Youtube-Android-App/blob/104c0a3e5d49d4584b8befa0858566575ed018a3/app/src/main/java/sangraula/sanjeeb/wissionapp/utils/YoutubeSearchHelper.java#L47
    private final URL getRequestUrl(String art, String tit) {
        final String PRIMER_URL = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=";
        final String SEGUN_URL = "%20video&maxResults=1&key=AIzaSyDvLLIvB9p3qU_yg7hI4jz0y-Hyy52YbRw";
        URL url = null;

        try {

            url = new URL(PRIMER_URL + art + "%20" + tit + SEGUN_URL);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;

    }

    public String buscarYT(String art, String tit) {
        URL url = getRequestUrl(art, tit);
        String infovideo= null;
        try {
            infovideo = new TareaBuscar().execute(url).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return infovideo;
    }

    private class TareaBuscar extends AsyncTask<URL, Void, String> {
        @Override
        protected String doInBackground(URL... urls) {
            String idVideo = "";
            String img = "";
            URL url = urls[0];
            StringBuilder response = new StringBuilder();
            HttpURLConnection httpconn = null;

            try {
                httpconn = (HttpURLConnection) url.openConnection();

                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    input.close();
                }
                JSONObject o1 = new JSONObject(response.toString());
                JSONArray a1 = o1.getJSONArray("items");
                JSONObject o2 = a1.getJSONObject(0);
                idVideo = o2.getJSONObject("id").getString("videoId");
                img = o2.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").getString("url");

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return idVideo+"¬"+img;
        }

    }
}