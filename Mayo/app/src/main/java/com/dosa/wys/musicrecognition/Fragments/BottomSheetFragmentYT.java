package com.dosa.wys.musicrecognition.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dosa.wys.musicrecognition.Auxiliar.BuscadorYoutube;
import com.dosa.wys.musicrecognition.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerUtils;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

public class BottomSheetFragmentYT extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;
    private YouTubePlayerView youTubePlayerView;
    private String idV = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.bottom_sheetyoutube, container, false);

        youTubePlayerView = vista.findViewById(R.id.youtube_player_view);

        setIdVideo();

        return vista;
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    public void setIdV(String idVideo) {
        this.idV = idVideo;
    }

    public void setIdVideo() {
        getLifecycle().addObserver(youTubePlayerView);

        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                YouTubePlayerUtils.loadOrCueVideo(
                        youTubePlayer, getLifecycle(),
                        idV,0f
                );
            }
        });
    }

    public String getInfo(String t, String a, int num){
        String videoInfo[];
        BuscadorYoutube buscadorYoutube = new BuscadorYoutube();
        String infovideo= buscadorYoutube.buscarYT(t,a);
        videoInfo = infovideo.split("¬");
        if (videoInfo.length != 0) {
            return videoInfo[num];
        } else
            return "Error";
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListener");
        }
    }
}