package com.dosa.wys.musicrecognition.Fragments;


import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dosa.wys.musicrecognition.Auxiliar.ConexionSQLite;
import com.dosa.wys.musicrecognition.R;
import com.dosa.wys.musicrecognition.Activities.ReconocerCancion;

/**
 * A simple {@link Fragment} subclass.
 */

public class InicioFragment extends Fragment {

    ImageView audio_grabado;


    public InicioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_inicio, container, false);

        //ConexionSQLite myDB = new ConexionSQLite(getContext());
        //myDB.deleteAllData();

        audio_grabado = vista.findViewById(R.id.record_audio);
        audio_grabado.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent grabarIntent = new Intent(getContext(), ReconocerCancion.class);

                startActivity(grabarIntent);

            }
        });

        return vista;
    }

}
